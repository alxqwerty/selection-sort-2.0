using System;

#pragma warning disable SA1611

namespace SelectionSort
{
    public static class Sorter
    {
        public static void SelectionSort(this int[] array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array), "Array cannot be null.");
            }

            for (int i = 0; i < array.Length; i++)
            {
                int latestIndexToSwap = 0;
                bool isSwapAvailable = false;

                for (int j = i + 1; j < array.Length; j++)
                {
                    while (j < array.Length)
                    {
                        if (array[i] > array[j])
                        {
                            isSwapAvailable = true;
                            latestIndexToSwap = j;
                        }

                        j++;
                    }

                    if (isSwapAvailable)
                    {
                        int container = array[i];
                        array[i] = array[latestIndexToSwap];
                        array[latestIndexToSwap] = container;

                        if (array[i] > array[i + 1])
                        {
                            i--;
                        }
                    }
                }
            }
        }

        public static void RecursiveSelectionSort(this int[] array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array), "Array cannot be null.");
            }

            int currentIndex = 0;
            int searchedIndex = currentIndex + 1;
            int indexOfMinValue = currentIndex;
            NextStep(array, ref currentIndex, ref searchedIndex, ref indexOfMinValue);
        }

        public static int[] NextStep(int[] array, ref int currentIndex, ref int searchedIndex, ref int indexOfMinValue)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array), "Array cannot be null.");
            }

            if (array.Length == 0)
            {
                return array;
            }

            if (currentIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(currentIndex));
            }

            if (searchedIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(searchedIndex));
            }

            if (indexOfMinValue < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(indexOfMinValue));
            }

            if (currentIndex == array.Length - 1)
            {
                return array;
            }

            if (array[indexOfMinValue] > array[searchedIndex])
            {
                indexOfMinValue = searchedIndex;

                if (indexOfMinValue == array.Length - 1)
                {
                    Swap(array, ref indexOfMinValue, ref currentIndex);
                    currentIndex++;
                    indexOfMinValue = currentIndex;
                    searchedIndex = currentIndex + 1;
                    return NextStep(array, ref currentIndex, ref searchedIndex, ref indexOfMinValue);
                }
                else
                {
                    searchedIndex++;
                    return NextStep(array, ref currentIndex, ref searchedIndex, ref indexOfMinValue);
                }
            }
            else if (array[indexOfMinValue] <= array[searchedIndex])
            {
                if (searchedIndex >= array.Length - 1)
                {
                    Swap(array, ref indexOfMinValue, ref currentIndex);
                    currentIndex++;
                    indexOfMinValue = currentIndex;
                    searchedIndex = currentIndex + 1;
                    return NextStep(array, ref currentIndex, ref searchedIndex, ref indexOfMinValue);
                }

                searchedIndex++;
                return NextStep(array, ref currentIndex, ref searchedIndex, ref indexOfMinValue);
            }

            return array;
        }

        public static int[] Swap(int[] array, ref int indexOfMinValue, ref int currentIndex)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array), "Array cannot be null.");
            }

            int container = array[indexOfMinValue];
            array[indexOfMinValue] = array[currentIndex];
            array[currentIndex] = container;

            return array;
        }
    }
}
